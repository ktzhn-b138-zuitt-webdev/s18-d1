/*Objects: 
	SYNTAX:
		let objectName = {
			keyA: valueA, 
			keyB: valueB
		}
*/

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: 1999
}

console.log('Result from creating objects using initializers/literal notation');
console.log(cellphone);
console.log(typeof cellphone);
console.log(cellphone.name);

// Creating objects using constructor function
/*
	- creates a reusable function to create several objects that have the same data structure
	- "this" keyword allows to assign a new object's properties by associating them with values received from a constructors function's parameter

	SYNTAX:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}
*/

function Laptop (name, manufactureDate){
	this['name'] = name; 
	// can do box notation but...better to use dot notation for this. better to not have spaces
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating objects using object constructors');
console.log(laptop);

let myLaptop = new Laptop('MacBook Air', 2020);
console.log('Result from creating objects using object constructors: ')
console.log(myLaptop);

// Accessing Object Properties 
// dot notation
console.log("Result from dot notation: " + myLaptop.name);
// Square bracket notation
console.log("Result from dot notation: " + myLaptop['name']);

// Accessing array objects

let arr = [laptop, myLaptop];

console.log(arr[0]['name']);
console.log(arr[0] .name);

// Initializing/Adding/Deleting/Reassigning Object Properties

let car = {}

car.name = "Honda Civic";
console.log('Result from adding properties using dot notation:');
console.log(car);

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log(car);

// Deleting object properties
delete car['manufacture date'];
console.log('Result from deleting properties: ');
console.log(car);

// Reassigning object properties
car.name = 'E46 BMW M3 GTR';
console.log('Result from reassigning properties: ');
console.log(car);

// Object Method
// Method is a function which is a property of an object

let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name);
	}
}

console.log(person);
console.log('Result from object methods');
person.talk();

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.')
}

person.walk();

// Add a run method to the person object
person.run = function(){
	console.log(this.name + ' ran 6km today.')
}
person.run();


// Creating reusable functions
let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);l
	}
}

friend.introduce();
console.log(friend.address);
console.log(friend.emails[1]);

// Real World Application of Objects
/* SCENARIO
	1. Create a game that would have several Pokemon interact with each other
	2. Every Pokemon would have the same set of status, properties, and function

*/


let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
};
console.log(myPokemon);

// Pokemon Object Constructor
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

console.log(pikachu);
pikachu.tackle(rattata);
